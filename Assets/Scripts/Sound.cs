﻿using UnityEngine;

[CreateAssetMenu]
public class Sound : ScriptableObject
{
    public AudioClip clip;

    [Range(0, 1)] public float volume = 1;
    [Range(0.1f, 3)] public float pitch = 1;

    public bool loop = false;
    public bool playOnSceneLoaded = false;
    [HideInInspector] public string sceneToPlay;

    public bool stopOnSceneUnloaded = false;
    [HideInInspector] public string sceneToStop;

    [HideInInspector] public AudioSource source;
}
