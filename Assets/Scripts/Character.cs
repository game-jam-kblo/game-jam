﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public CharColor characterColor = CharColor.NONE;
    public Animator animator;
    public Renderer _renderer;
    public GameObject balloon;

    public void CheckUnload()
    {
        if (transform.position.z <= CrowdManager.camTransform.position.z + 3 || transform.position.y <= -5)
        {
            if (CrowdManager.activeCharacters.Contains(this))
                CrowdManager.activeCharacters.Remove(this);
            else
                CrowdManager.attachedCharacters.Remove(this);

            CrowdManager.inactiveCharacters.Add(this);
            gameObject.SetActive(false);
        }
    }
}
