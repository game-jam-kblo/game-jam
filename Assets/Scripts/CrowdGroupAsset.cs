﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CrowdGroupAsset : ScriptableObject
{
    public List<CrowdSpawn> spawns = new List<CrowdSpawn>();
}

[System.Serializable]
public class CrowdSpawn
{
    public Vector3 spawnPosition;
    public CharColor characterColor;

    public CrowdSpawn(Vector3 position)
    {
        spawnPosition = position;
        characterColor = CharColor.NONE;
    }
}
