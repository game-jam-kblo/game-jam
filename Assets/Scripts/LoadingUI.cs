﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingUI : MonoBehaviour
{
    [SerializeField] private FloatVariable loadingProgress = null;
    [SerializeField] private Slider slider = null;

    private void Update()
    {
        slider.value = loadingProgress.Value;
    }
}
