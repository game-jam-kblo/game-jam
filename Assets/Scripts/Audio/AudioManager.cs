﻿using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    private AudioEventListener audioEventListener;

    [Header("Played by Scene")]
    public Sound[] sounds;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        audioEventListener = GetComponent<AudioEventListener>();

        foreach(Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;

            s.source.loop = s.loop;
        }

        foreach(AEvent s in audioEventListener.Events)
        {
            s.AudioEvent.sound.source = gameObject.AddComponent<AudioSource>();
            s.AudioEvent.sound.source.clip = s.AudioEvent.sound.clip;

            s.AudioEvent.sound.source.volume = s.AudioEvent.sound.volume;
            s.AudioEvent.sound.source.pitch = s.AudioEvent.sound.pitch;

            s.AudioEvent.sound.source.loop = s.AudioEvent.sound.loop;
        }
    }

    public void PlayEvent (AudioEvent audioToPlay)
    {
        AEvent s = Array.Find(audioEventListener.Events, aEvent => aEvent.AudioEvent.name == audioToPlay.name);

        if(s == null)
        {
            Debug.LogWarning("Sound: " + audioToPlay.name + " not found!");
            return;
        }

        s.AudioEvent.sound.source.Play();
    }

    private void Play(Sound soundToPlay, bool value)
    {
        Sound s = Array.Find(sounds, sound => sound == soundToPlay);

        if (s == null)
        {
            Debug.LogWarning("Sound: " + soundToPlay.name + " not found!");
            return;
        }

        if (value)
            s.source.Play();
        else
            s.source.Stop();
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.sceneUnloaded += OnSceneUnloaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        SceneManager.sceneUnloaded -= OnSceneUnloaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        foreach(Sound sound in sounds)
        {
            if (sound.sceneToPlay.Equals(scene.name))
                Play(sound, true);
        }
    }

    void OnSceneUnloaded(Scene scene)
    {
        foreach (Sound sound in sounds)
        {
            if (sound.sceneToPlay.Equals(scene.name))
                Play(sound, false);
        }
    }

}
