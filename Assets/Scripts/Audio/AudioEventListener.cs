﻿using UnityEngine;
using UnityEngine.Events;

public class AudioEventListener : MonoBehaviour
{
    public AEvent[] Events;

    private void OnEnable()
    {
        for (int i = 0; i < Events.Length; i++)
        {
            Events[i].AudioEvent.RegisterListener(this);
        }

    }

    private void OnDisable()
    {
        for (int i = 0; i < Events.Length; i++)
        {
            Events[i].AudioEvent.UnregisterListener(this);
        }
    }

    public void OnEventRaised(string identifier)
    {
        for (int i = 0; i < Events.Length; i++)
        {
            if (Events[i].AudioEvent.name.Equals(identifier))
                Events[i].Response.Invoke(Events[i].AudioEvent);
        }
    }
}

[System.Serializable]
public class AEvent
{
#if UNITY_EDITOR
    public string name;
#endif

    public AudioEvent AudioEvent;
    public AudioResponseEvent Response;
}

[System.Serializable]
public class AudioResponseEvent : UnityEvent<AudioEvent>
{

}

