﻿using UnityEngine;
using TMPro;

public class GateTile : Tile
{
    [SerializeField] private Gate[] gates = null;

    private bool activated = false;

    private void OnEnable()
    {
        Gate firstGate = gates[Random.Range(0, gates.Length - 1)];

        firstGate.requiredCharacters = 1;

        foreach (TextMeshPro text in firstGate.texts)
            text.text = firstGate.requiredCharacters.ToString();

        firstGate.animation.gameObject.SetActive(true);

        foreach (Gate gate in gates)
        {
            if (!gate.animation.gameObject.activeInHierarchy)
            {
                int amount = Random.Range(1, 4) * 5;
                gate.requiredCharacters = amount;
                foreach (TextMeshPro text in gate.texts)
                    text.text = amount.ToString();

                gate.animation.gameObject.SetActive(true);
            }

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Detection"))
        {
            if (!activated)
            {
                foreach (Gate gate in gates)
                {
                    if (gate.requiredCharacters <= CrowdManager.attachedCharacters.Count)
                    {
                        gate.animation.Play();

                        foreach (BoxCollider boxCollider in gate.doors)
                            boxCollider.enabled = false;
                    }

                }

                activated = true;
            }
        }
    }

    private void OnDisable()
    {
        foreach (Gate gate in gates)
        {
            gate.animation.Rewind();
            gate.animation.gameObject.SetActive(false);

            foreach (BoxCollider boxCollider in gate.doors)
                boxCollider.enabled = true;
        }

        activated = false;
    }

}

[System.Serializable]
public class Gate
{
    public TextMeshPro[] texts;
    public Animation animation;
    public BoxCollider[] doors;
    [HideInInspector] public int requiredCharacters;
}
