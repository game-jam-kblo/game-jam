﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleTile : Tile
{
    private void OnEnable()
    {
        transform.GetChild(Random.Range(0, transform.childCount)).gameObject.SetActive(true);

        for (int i = 0; i < transform.childCount; i++)
        {
            Transform _transform = transform.GetChild(i);

            if (!_transform.gameObject.activeInHierarchy)
                _transform.gameObject.SetActive(Random.value > 0.5f);
        }
    }

    private void OnDisable()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }


}
