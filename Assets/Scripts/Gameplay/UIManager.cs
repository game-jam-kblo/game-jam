﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI crowdText = null;
    [SerializeField] private TextMeshProUGUI requiredText = null;

    [Space]

    [SerializeField] private GameObject nextLevelButton = null;
    [SerializeField] private GameObject menuButton = null;

    [Space]

    public GameEvent endGameEvent;
    public GameEvent changeColor;

    [Space]

    public FloatVariable CurrentLevel;

    private void Start()
    {
        requiredText.text = "Required crowd: " + PathManager.requiredCrowd.ToString();

        if(CurrentLevel.Value == LevelManager.levelAmount)
        {
            nextLevelButton.SetActive(false);
            menuButton.SetActive(true);
        }
    }

    private void Update()
    {
        crowdText.text = CrowdManager.attachedCharacters.Count.ToString();

        if (CrowdManager.attachedCharacters.Count == 0 && Player.GameState != GameState.END)
            endGameEvent.Raise();
    }

    public void ChangeColor(int color)
    {
        CrowdManager.currentColor = (CharColor)color;
        changeColor.Raise();
    }

}
