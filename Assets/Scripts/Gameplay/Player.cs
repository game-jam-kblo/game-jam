﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    PLAY,
    PAUSE,
    END,
    SUBWAY
}

public class Player : MonoBehaviour
{
    [Header("Player Variables")]
    [SerializeField] private float horizontalSpeed = 0;
    [SerializeField] private float verticalSpeed = 0;
    [SerializeField] private float colorSwitchTime = 0;
    [SerializeField] private Transform horizontalTransform = null;

    [Space]

    public GameEvent movedToNextTile;
    public Material playerMaterial;

    private float horizontal;
    private Coroutine backToNoneCoroutine = null;

    public static int currentPos = 0;
    public static GameState GameState;

    private void Awake()
    {
        GameState = GameState.PLAY;
        currentPos = 0;
    }

    private void Start()
    {
        GetComponentInChildren<Animator>().SetFloat("Speed", 1);
    }

    void Update()
    {
        if(GameState == GameState.PLAY)
        {
            horizontal = Input.GetAxis("Horizontal");

            horizontalTransform.Translate(Vector3.right * horizontalSpeed * horizontal * Time.deltaTime);
            transform.Translate(Vector3.forward * verticalSpeed * Time.deltaTime);

            if (transform.position.z > currentPos + PathManager.nextTileDistance)
            {
                movedToNextTile.Raise();
                currentPos = Mathf.RoundToInt(transform.position.z);
            }

        }

    }

    private IEnumerator BackToNone()
    {
        yield return new WaitForSeconds(colorSwitchTime);
        CrowdManager.currentColor = CharColor.NONE;
        playerMaterial.color = Color.white;
        backToNoneCoroutine = null;
    }

    public void ChangeColor()
    {
        switch (CrowdManager.currentColor)
        {
            case CharColor.BLUE:
                playerMaterial.color = Color.cyan;
                break;
            case CharColor.GREEN:
                playerMaterial.color = Color.green;
                break;
            case CharColor.YELLOW:
                playerMaterial.color = Color.yellow;
                break;
        }

        if(backToNoneCoroutine == null)
        {
            backToNoneCoroutine = StartCoroutine(BackToNone());
        }

        else
        {
            StopCoroutine(backToNoneCoroutine);
            backToNoneCoroutine = StartCoroutine(BackToNone());
        }

    }

    private void OnDisable()
    {
        playerMaterial.color = Color.white;
    }

    public void ApplyMaterial(GameObject character)
    {
        character.GetComponentInChildren<Renderer>().material = playerMaterial;
    }

    public void EndGame()
    {
        GameState = GameState.END;
    }
}
