﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Path : ScriptableObject
{
    public bool handMade;

    public int pathSize;

    public int requiredCrowd;

    [Header("Tiles Setup")]
    public TileContainer[] tileContainer;

    public TilePool[] pools;

}

[System.Serializable]
public class TilePool
{
    public GameObject Tile;
    public int poolSize;
}

[System.Serializable]
public class TileContainer
{
    public Tile tile;
    public CrowdGroupAsset crowd;
}
