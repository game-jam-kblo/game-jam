﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharColor
{
    BLUE,
    GREEN, 
    YELLOW,
    NONE = 999
}

public class CrowdManager : MonoBehaviour
{
    [SerializeField] private int crowdPoolSize = 0;
    [SerializeField] private GameObject character = null;
    [SerializeField] private Transform inactiveTransform = null;
    [SerializeField] private Transform attachedTransform = null;
    [SerializeField] private Transform cameraTransform = null;
    [SerializeField] private Material[] materials = null;
    [SerializeField] private Material[] ballonMaterial = null;

    [Header("Subway Config")]
    [SerializeField] private int enterDistance = 0;

    public static Material[] staticMaterials = null;
    public static Material[] staticBallonMaterials = null;
    public static Transform camTransform = null;

    private Player player;
    public static CharColor currentColor = CharColor.NONE;

    public static List<Character> attachedCharacters = new List<Character>();
    public static List<Character> activeCharacters = new List<Character>();
    public static List<Character> inactiveCharacters = new List<Character>();

    void Awake()
    {
        attachedCharacters.Clear();
        activeCharacters.Clear();
        inactiveCharacters.Clear();
        currentColor = CharColor.NONE;

        player = GetComponent<Player>();

        materials[0].color = Color.cyan;
        materials[1].color = Color.green;
        materials[2].color = Color.yellow;

        staticMaterials = materials;
        staticBallonMaterials = ballonMaterial;
        camTransform = cameraTransform;

        for (int i = 0; i < crowdPoolSize; i++)
        {
            Character _character = Instantiate(character, inactiveTransform).GetComponent<Character>();
            _character.gameObject.SetActive(false);
            inactiveCharacters.Add(_character);
        }

        attachedCharacters.Add(GetComponentInChildren<Character>());
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && currentColor != CharColor.NONE)
        {
            Character character = other.GetComponent<Character>();

            if(character.characterColor == currentColor && !attachedCharacters.Contains(character))
            {
                other.transform.parent = attachedTransform;
                character.animator.SetFloat("Speed", 1);
                attachedCharacters.Add(character);
                player.ApplyMaterial(other.gameObject);
                character.balloon.SetActive(false);
                activeCharacters.Remove(character);
            }
            
        }

    }

    private void Update()
    {
        if(Player.GameState == GameState.SUBWAY)
        {
            attachedTransform.Translate(Vector3.forward * enterDistance * Time.deltaTime);
        }
    }

    public static void FakeInstantiate(Vector3 position, CharColor charColor)
    {
        if(inactiveCharacters?.Count == 0)
        {
            Debug.LogWarning("POOL DE PERSONAGENS CHEIA");
            return;
        }

        inactiveCharacters[0].transform.position = position;

        switch (charColor)
        {
            case CharColor.BLUE:
                inactiveCharacters[0]._renderer.material = staticMaterials[0];
                inactiveCharacters[0].characterColor = CharColor.BLUE;
                inactiveCharacters[0].balloon.GetComponent<ParticleSystemRenderer>().material = staticBallonMaterials[(int)charColor];
                inactiveCharacters[0].balloon.SetActive(true);
                break;
            case CharColor.GREEN:
                inactiveCharacters[0]._renderer.material = staticMaterials[1];
                inactiveCharacters[0].characterColor = CharColor.GREEN;
                inactiveCharacters[0].balloon.GetComponent<ParticleSystemRenderer>().material = staticBallonMaterials[(int)charColor];
                inactiveCharacters[0].balloon.SetActive(true);
                break;
            case CharColor.YELLOW:
                inactiveCharacters[0]._renderer.material = staticMaterials[2];
                inactiveCharacters[0].characterColor = CharColor.YELLOW;
                inactiveCharacters[0].balloon.GetComponent<ParticleSystemRenderer>().material = staticBallonMaterials[(int)charColor];
                inactiveCharacters[0].balloon.SetActive(true);
                break;
            case CharColor.NONE:
                int index = Random.Range(0, staticMaterials.Length);
                inactiveCharacters[0]._renderer.material = staticMaterials[index];
                inactiveCharacters[0].characterColor = (CharColor)index;
                inactiveCharacters[0].balloon.GetComponent<ParticleSystemRenderer>().material = staticBallonMaterials[index];
                inactiveCharacters[0].balloon.SetActive(true);
                break;
        }

        inactiveCharacters[0].gameObject.SetActive(true);
        activeCharacters.Add(inactiveCharacters[0]);
        inactiveCharacters.Remove(inactiveCharacters[0]);
    }

    public static void EnterSubway()
    {
        foreach(Character character in attachedCharacters)
        {
            character.gameObject.layer = 8;
        }

        Player.GameState = GameState.SUBWAY;
    }

    public static void StopSubway()
    {
        Player.GameState = GameState.END;

        foreach (Character character in attachedCharacters)
            character.animator.SetFloat("Speed", 0);
    }

    public static void Leave()
    {
        foreach (Character character in attachedCharacters)
        {
            character.transform.parent = PathManager.subwayParent;
            character.animator.SetFloat("Speed", 0);
        }
    }

}
