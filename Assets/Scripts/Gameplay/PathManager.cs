﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PathManager : MonoBehaviour
{
    [Header("Global Setup")]
    [SerializeField] private int tilePoolSize = 0;

    [Space]

    [HideInInspector] public Path Path;
    [SerializeField] private GameObject tile = null;
    [SerializeField] private GameObject subwayTile = null;
    [SerializeField] private RuntimeAnimatorController subwayWinAnimator = null;
    [SerializeField] private AnimatorOverrideController subwayLossAnimator = null;
    [SerializeField] private TextMeshPro[] subwayTexts = null;

    [Space]

    public GameEvent endGameEvent;
    public GameEvent victoryEvent;

    public GameObject character;

    private int position;
    private int childToMove;
    private int lastChild;

    public static int nextTileDistance;
    private int loadedTiles;

    public static List<Tile> inactiveTiles = new List<Tile>();
    public static List<Tile> activeTiles = new List<Tile>();
    public static int requiredCrowd = 0;
    public static Transform subwayParent = null;


    private void Awake()
    {
        inactiveTiles.Clear();
        activeTiles.Clear();
        loadedTiles = 0;
        subwayParent = subwayTile.transform.GetChild(0);
    }

    private void Start()
    {
        position = -4;
        childToMove = 0;
        lastChild = tilePoolSize - 1;
        nextTileDistance = (Path.tileContainer[0].tile.tileLenght + Path.tileContainer[1].tile.tileLenght) / 2;
        requiredCrowd = Path.requiredCrowd;

        foreach (TextMeshPro textMeshPro in subwayTexts)
        {
            textMeshPro.text = requiredCrowd.ToString();
        }

        if (!Path.handMade)
        {
            for (int i = 0; i < tilePoolSize; i++)
            {
                Instantiate(tile, new Vector3(0, -2, position), Quaternion.identity, this.transform);
                position += 4;
                loadedTiles++;
                nextTileDistance = 4;
            }
        }

        else
        {
            foreach (TilePool tilePool in Path.pools)
            {
                for (int i = 0; i < tilePool.poolSize; i++)
                {
                    GameObject gameObject = Instantiate(tilePool.Tile, this.transform);
                    gameObject.SetActive(false);
                    inactiveTiles.Add(gameObject.GetComponent<Tile>());
                }
            }

            for (int i = 0; i < tilePoolSize; i++)
            {

                for (int j = 0; j < inactiveTiles.Count; j++)
                {
                    var tile = inactiveTiles[j].GetType();

                    if (tile == Path.tileContainer[i].tile.GetType())
                    {
                        inactiveTiles[j].transform.position = new Vector3(0, -2, position);
                        inactiveTiles[j].gameObject.SetActive(true);
                        activeTiles.Add(inactiveTiles[j]);

                        SpawnCrowd(i, j);

                        inactiveTiles.Remove(inactiveTiles[j]);
                        break;
                    }

                }

                loadedTiles++;

                if (loadedTiles == Path.tileContainer.Length)
                    break;

                position += (Path.tileContainer[i].tile.tileLenght + Path.tileContainer[i+1].tile.tileLenght)/2;
            }

        }
        
    }

    public void LoadNextTile()
    {
        if (!Path.handMade)
        {
            if (loadedTiles < Path.pathSize)
            {
                transform.GetChild(childToMove).transform.position = transform.GetChild(lastChild).transform.position + new Vector3(0, 0, 4);
                loadedTiles++;

                if (childToMove < tilePoolSize - 1)
                {
                    lastChild = childToMove;
                    childToMove++;
                }

                else
                {
                    childToMove = 0;
                    lastChild = tilePoolSize - 1;
                }
            }

            else
            {
                loadedTiles++;

                if (loadedTiles > Path.pathSize + 3)
                    EndGame();
            }
        }

        else
        {
            if(loadedTiles < Path.tileContainer.Length)
            {
                activeTiles[0].gameObject.SetActive(false);
                inactiveTiles.Add(activeTiles[0]);
                activeTiles.Remove(activeTiles[0]);

                var nextTile = Path.tileContainer[loadedTiles].tile.GetType();

                for (int i = 0; i < inactiveTiles.Count; i++)
                {
                    var tile = inactiveTiles[i].GetType();

                    if(tile == nextTile)
                    {
                        inactiveTiles[i].transform.position = activeTiles[activeTiles.Count - 1].transform.position + new Vector3(0, 0, (Path.tileContainer[loadedTiles - 1].tile.tileLenght + Path.tileContainer[loadedTiles].tile.tileLenght) / 2);
                        inactiveTiles[i].gameObject.SetActive(true);

                        activeTiles.Add(inactiveTiles[i]);

                        SpawnCrowd(loadedTiles, i);

                        inactiveTiles.Remove(inactiveTiles[i]);

                        break;
                    }

                }

                nextTileDistance = activeTiles[0].tileLenght;
                loadedTiles++;
            }

            else
            {
                if (!subwayTile.activeInHierarchy)
                {
                    subwayTile.transform.position = Vector3.forward * (activeTiles[activeTiles.Count - 1].transform.position.z + activeTiles[activeTiles.Count - 1].tileLenght);
                    subwayTile.SetActive(true);
                }

                if (Player.currentPos > activeTiles[activeTiles.Count - 3].transform.position.z)
                    StartCoroutine(EndGame());
            }
        }

    }

    public IEnumerator EndGame()
    {
        Animator animator = subwayTile.GetComponent<Animator>();

        if (CrowdManager.attachedCharacters.Count >= Path.requiredCrowd)
            animator.runtimeAnimatorController = subwayWinAnimator;

        else
            animator.runtimeAnimatorController = subwayLossAnimator;

        animator.SetTrigger("Open");  

        if (CrowdManager.attachedCharacters.Count >= Path.requiredCrowd)
        {
            CrowdManager.EnterSubway();

            yield return new WaitForSeconds(3f);

            animator.SetTrigger("Close");

            yield return new WaitForSeconds(1f);

            CrowdManager.Leave();
            animator.SetTrigger("Leave");

            yield return new WaitForSeconds(2f);

            victoryEvent.Raise();
        }


        else
        {
            CrowdManager.StopSubway();

            yield return new WaitForSeconds(3f);

            animator.SetTrigger("Close");

            yield return new WaitForSeconds(1f);

            animator.SetTrigger("Leave");

            yield return new WaitForSeconds(2f);

            endGameEvent.Raise();
        }


    }

    private void SpawnCrowd(int currentTile, int transformTile)
    {
        if (Path.tileContainer[currentTile].crowd != null)
        {
            CrowdGroupAsset crowdSpawn = Path.tileContainer[currentTile].crowd;

            for (int x = 0; x < crowdSpawn.spawns.Count; x++)
            {
                CrowdManager.FakeInstantiate(crowdSpawn.spawns[x].spawnPosition + (Vector3.forward * inactiveTiles[transformTile].transform.position.z), crowdSpawn.spawns[x].characterColor);
            }
        }
    }
}
