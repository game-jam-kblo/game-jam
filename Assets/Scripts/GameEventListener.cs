﻿using UnityEngine;
using UnityEngine.Events;

public class GameEventListener : MonoBehaviour
{
    public Event[] Events;

    private void OnEnable()
    {
        for (int i = 0; i < Events.Length; i++)
        {
            Events[i].GameEvent.RegisterListener(this);
        }
        
    }

    private void OnDisable()
    {
        for (int i = 0; i < Events.Length; i++)
        {
            Events[i].GameEvent.UnregisterListener(this);
        }
    }

    public void OnEventRaised(string identifier)
    {
        for (int i = 0; i < Events.Length; i++)
        {
            if (Events[i].GameEvent.name.Equals(identifier))
                Events[i].Response.Invoke();
        }
    }
}

[System.Serializable]
public class Event
{
#if UNITY_EDITOR
    public string name;
#endif

    public GameEvent GameEvent;
    public UnityEvent Response;
}