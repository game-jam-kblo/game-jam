﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FlowManager : MonoBehaviour
{
    [SerializeField] private FloatVariable loadingProgress = null;
    [SerializeField] private FloatVariable sceneToLoad = null;

    private void Awake()
    {
        DontDestroyOnLoad(this);
        SceneManager.LoadScene(2);
    }

    public void LoadLevel()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
        StartCoroutine(LoadAsynchrounously((int)sceneToLoad.Value));    
    }

    IEnumerator LoadAsynchrounously (int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            loadingProgress.SetValue(progress);

            yield return null;
        }
    }

    public void Close()
    {
#if UNITY_EDITOR

        UnityEditor.EditorApplication.isPlaying = false;

#endif

        Application.Quit();
    }

    public void Pause()
    {
        Time.timeScale = 0;
    }

    public void Resume()
    {
        Time.timeScale = 1;
    }
}
