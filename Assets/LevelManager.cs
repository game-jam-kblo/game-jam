﻿using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public FloatVariable currentLevel;

    [SerializeField] private PathManager PathManager = null;
    [SerializeField] private Path[] levels = null;

    public static int levelAmount;

    private void Awake()
    {
        levelAmount = levels.Length;
    }

    private void Start()
    {
        PathManager.Path = levels[(int)currentLevel.Value - 1];
    }
}
