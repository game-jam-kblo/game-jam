﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CrowdGroup))]
public class CrowdGroupEditor : Editor
{
    public override void OnInspectorGUI()
    {

        DrawDefaultInspector();

        var cg = target as CrowdGroup;

        if (GUILayout.Button("Save Transforms"))
        {
            cg.CrowdGroupAsset.spawns.Clear();

            for (int i = 0; i < cg.transform.childCount -1; i++)
            {
                cg.CrowdGroupAsset.spawns.Add(new CrowdSpawn(cg.transform.GetChild(i + 1).transform.position));
            }

            EditorUtility.SetDirty(cg.CrowdGroupAsset);
            AssetDatabase.SaveAssets();
        }

        if (GUILayout.Button("Clear"))
        {
            cg.CrowdGroupAsset.spawns.Clear();
            EditorUtility.SetDirty(cg.CrowdGroupAsset);
            AssetDatabase.SaveAssets();

        }
            
    }
}
