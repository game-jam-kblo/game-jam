﻿using UnityEditor;

[CustomEditor(typeof(Sound))]
public class SoundEditor : Editor
{
    private SerializedProperty sceneToPlay;
    private SerializedProperty sceneToStop;

    private void OnEnable()
    {
        sceneToPlay = serializedObject.FindProperty("sceneToPlay");
        sceneToStop = serializedObject.FindProperty("sceneToStop");
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var sound = target as Sound;

        serializedObject.Update();

        if (sound.playOnSceneLoaded)
        {
            EditorGUILayout.PropertyField(sceneToPlay);
        }

        if (sound.stopOnSceneUnloaded)
        {
            EditorGUILayout.PropertyField(sceneToStop);
        }

        serializedObject.ApplyModifiedProperties();
    }

    
}
