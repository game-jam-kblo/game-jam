﻿using UnityEditor;
using UnityEditor.SceneManagement;

public static class EditorTools
{
    public const string CURRENT_SCENE = "currentScene";
    public const string PREVIEW = "preview";

    [MenuItem("Game/Preview")]
    static void Preview()
    {
        if (!EditorApplication.isPlaying)
        {
            EditorPrefs.SetString(CURRENT_SCENE, EditorSceneManager.GetActiveScene().path);
            EditorPrefs.SetBool(PREVIEW, true);
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene("Assets/Scenes/Start.unity");
            EditorApplication.EnterPlaymode();
        }

    }

}

[InitializeOnLoad]
public static class PlayStateNotifier
{

    static PlayStateNotifier()
    {
        EditorApplication.playModeStateChanged += ModeChanged;
    }

    static void ModeChanged(PlayModeStateChange playModeState)
    {
        if (playModeState == PlayModeStateChange.EnteredEditMode && EditorPrefs.GetBool(EditorTools.PREVIEW) == true)
        {
            EditorSceneManager.OpenScene(EditorPrefs.GetString(EditorTools.CURRENT_SCENE));
            EditorPrefs.SetBool(EditorTools.PREVIEW, false);
        }
    }
}


